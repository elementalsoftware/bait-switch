"use strict";

const _ = require("lodash");
const express = require("express");
const app = express();
const http = require("http").Server(app);
const socketio = require("socket.io")(http);
const bodyParser = require('body-parser');
const appConfig = require("./config.json");
const requireDir = require("require-dir");
const dataModel = require("./data-model");
const rpcRouter = require("./rpc-router");

const deviceDrivers = requireDir("./deviceDrivers");
const commDrivers = requireDir("./commDrivers");


let driverInstances = [];
let model = dataModel.model;
appConfig.devices.forEach((deviceConfig, deviceId) => {
	if(!commDrivers.hasOwnProperty(deviceConfig.commDriver)){
		throw new Error("Invalid comm driver: " + deviceConfig.commDriver);
	}
	if(!deviceDrivers.hasOwnProperty(deviceConfig.type)){
		throw new Error("Invalid device type: " + deviceConfig.type);
	}
	if(model.deviceState.length != deviceId || model.deviceConfig.length != deviceId){
		throw new Error("Assertion failed");
	}
	let commDriver = new commDrivers[deviceConfig.commDriver](deviceConfig.commAddr);
	model.deviceState.push({connected: false});
	model.deviceConfig.push(deviceConfig);
	driverInstances.push(new deviceDrivers[deviceConfig.type](commDriver, dataModel, deviceId));
});


const port = appConfig.port || 8080;

app.use("/", express.static("frontend/dist"));
app.use(bodyParser.json());

app.get("/api", function(req, res){
	res.json(model);
});

app.get("/api/rpc", function(req, res){
	res.json(rpcRouter.getDescriptor());
});

app.post("/api/rpc/:rpcId", function(req, res){
	if(!rpcRouter.hasRpc(req.params.rpcId)){
		res.status(404).json({success: false, msg: "rpc method " + req.params.rpcId + " is not available"});
		return;
	}
	rpcRouter.execRpc(req.params.rpcId, req.body, (err) => {
		if(err){
			res.status(500).json({success: false, msg: err});
		}else{
			res.json({success: true});
		}
	});
});

socketio.on('connection', function(socket){
	let subscribedEvents = [];
	socket.on("subscribe", objPath => {
		let handler = (events) => socket.emit("change", events);
		dataModel.on(objPath, handler);
		subscribedEvents.push([objPath, handler]);

		let currentValue = model;
		let pathArray = _.toPath(objPath);
		while(pathArray.length && currentValue){
			currentValue = currentValue[pathArray.pop()];
		}
		if(pathArray.length){
			currentValue = undefined;
		}

		socket.emit("subscribed", objPath, currentValue);
	});
	socket.on("rpc", (rpcId, requestId, args) => {
		if(!rpcRouter.hasRpc(rpcId)){
			if(requestId){
				socket.emit("rpc-result", requestId, false, "rpc method " + rpcId + " is not available");
			}
			return;
		}
		rpcRouter.execRpc(rpcId, args, (err) => {
			if(requestId){
				if(err){
					socket.emit("rpc-result", requestId, false, err);
				}else{
					socket.emit("rpc-result", requestId, true);
				}
			}
		});
	});
	socket.on("close", () => {
		subscribedEvents.forEach(([eventName, handler]) => dataModel.removeListener(eventName, handler));
	});
});

http.listen(port, function(){
	console.log("Application listening on " + port);
});

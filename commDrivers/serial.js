"use strict";

const EventEmitter = require("events").EventEmitter;
const SerialPort = require("serialport");

class SerialDriver extends EventEmitter {
    constructor(connectionString){
        super();
		this._connectionString = connectionString;
		this._conn = null;
    }

    connect(){
		if(this._conn === null){
			this._conn = new SerialPort(this._connectionString, {}, (err) => {
				if(err){
					this.emit("disconnected");
				}else{
					this.emit("connected");
				}
			});

			this._conn.on("data", (data) => this.emit("dataReceived", data));
		}
    }

    sendRequest(request, completeCallback){
		if(this._conn !== null){
			if(!(request instanceof Buffer)){
				request = Buffer.from(request);
			}
			this._conn.write(request, completeCallback);
		}
    }
}

module.exports = SerialDriver;

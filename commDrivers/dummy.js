"use strict";

const EventEmitter = require("events").EventEmitter;

class DummyDriver extends EventEmitter {
    constructor(connectionString){
        super();

        setTimeout(() => this.emit("connected"), 200);
    }

    connect(){
        
    }

    sendRequest(request, completeCallback){
        console.log("Dummy comm driver sendRequest: '" + request + "'");
        if(completeCallback){
            setTimeout(completeCallback, 30);
        }
    }
}

module.exports = DummyDriver;

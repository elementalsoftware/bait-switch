"use strict";

const RpcBase = require("../rpc-base");

class Vp728Blank extends RpcBase {
    constructor(dataModel){
        super(dataModel);
    }
    exec(execArgs, callback){
        if(!this.validate(execArgs, callback)){
            return;
        }
        
        this.getState(execArgs.deviceId).blank = execArgs.blank;
        return this.success(callback);
    }

    getValidDeviceTypes(){
        return ["vp-728"];
    }

    getValidationSchema(){
        return {
            type: "object",
            properties: {
                deviceId: {
                    type: "integer",
                    minimum: 0
                },
                blank: {
                    type: "boolean"
                }
            },
            required: ["deviceId", "blank"]
        }
    }
}

module.exports = Vp728Blank;
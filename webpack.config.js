const path               = require('path');
const webpack            = require('webpack');
const HtmlWebpackPlugin  = require('html-webpack-plugin');
const ExtractTextPlugin  = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const SRC_DIR          = './frontend/src/';
const DIST_DIR         = './frontend/dist/';

module.exports = {
	entry: {
		'polyfills': SRC_DIR + 'polyfills.ts',
		'vendor': SRC_DIR + 'vendor.ts',
		'app': SRC_DIR + 'main.ts'
	},
	resolve: {
		extensions: ['.ts', '.js']
	},
	output: {
		path: path.resolve(DIST_DIR),
		publicPath: '/',
		filename: '[name].[hash].js',
		chunkFilename: '[id].[hash].chunk.js'
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				loaders: [
					{
						loader: 'ts-loader',
						options: { 
							configFile: SRC_DIR + 'tsconfig.json' 
						}
					}, 
					'angular2-template-loader'
				]
			},
			{
				test: /\.html$/,
				loader: 'html-loader'
			},
			{
				test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
				loader: 'file-loader?name=assets/[name].[hash].[ext]'
			},
			{
			test: /\.s?css$/,
				exclude: path.resolve(SRC_DIR + "app/"),
				loader: ExtractTextPlugin.extract({ 
					fallback: 'style-loader', 
					use: [
						{
							loader: 'css-loader?sourceMap'
						},
						{
							loader: 'sass-loader',
							options: {
								includePaths: [
									path.resolve(SRC_DIR + "styles/")
								]
							}
						}
					] 
				})
			},
			{
			test: /\.s?css$/,
				include: path.resolve(SRC_DIR + "app/"),
				use: [
					{
						loader: 'raw-loader'
					},
					{
						loader: 'sass-loader',
						options: {
							includePaths: [
								path.resolve(SRC_DIR + "styles/")
							]
						}
					}
				]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin([DIST_DIR]),
		// Workaround for angular/angular#11580
		new webpack.ContextReplacementPlugin(
			/angular(\\|\/)core(\\|\/)@angular/,
			'./frontend/src', 
			{} // a map of your routes
		),

		new ExtractTextPlugin("[name].[hash].css"),

		new webpack.optimize.CommonsChunkPlugin({
			name: ['app', 'vendor', 'polyfills']
		}),

		new HtmlWebpackPlugin({
			template: SRC_DIR + 'index.html'
		})
	]
};

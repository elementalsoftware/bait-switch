import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavigationBar } from './components/navbar/navbar.component';

import { DataService } from "./data.service";

@NgModule({
  declarations: [
    AppComponent,
    NavigationBar
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }

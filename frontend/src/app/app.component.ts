import { Component } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'this test application';

  constructor(private dataService : DataService){}

  onClick() : void {
    this.dataService.fireRpc("vp728.blank", {deviceId: 0, blank: !this.isBlank()});
  }

  isBlank() : boolean {
    let data = this.dataService.data;
    if(data.hasOwnProperty("deviceState") && data.deviceState.length > 0){
      return !!data.deviceState[0].blank;
    }
  }
}

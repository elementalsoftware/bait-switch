import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import * as io from 'socket.io-client';

@Injectable()
export class DataService {
	private socket : SocketIOClient.Socket;
	private rpcCallbacks : Array<Array<Function>> = [];
	readonly connectPromise : Promise<void>;
	private _data : any = {};
	constructor(){
		this.socket = io();
		this.connectPromise = new Promise((resolve, reject) => {
			this.socket.on("connect", () => {
				this.socket.emit("subscribe", "");
			});
			this.socket.on("subscribed", (path : String, currentValue : Object) => {
				this._data = currentValue;
				resolve();
			});
		});
		
		this.socket.on("change", (events : Array<Array<any>>) => {
			//Handle data model change
			console.log("Model change", events);
			events.forEach(([eventPath, oldValue, newValue]) => {
				if(eventPath.length === 0){
					this._data = newValue;
					return;
				}
				let finalPath = eventPath.pop();
				let base = this._data;
				for(let pathComponent of eventPath){
					base = base[pathComponent];
					if(base == null){
						console.warn("Could not locate change path", eventPath);
						return;
					}
				}
				base[finalPath] = newValue;
			});
			console.log("model is now", this._data);
		});
		this.socket.on("rpc-result", (requestId : number, result : boolean, err : string) => {
			if(this.rpcCallbacks.hasOwnProperty(requestId)){
				let promiseAnswers = this.rpcCallbacks[requestId];
				if(result){
					promiseAnswers[0]();
				}else{
					promiseAnswers[1](err);
				}
				delete this.rpcCallbacks[requestId];
			}
		});
	}

	get data() : any {
		return this._data;
	}

	fireRpc(rpcId : string, args : object) : void {
		console.log("Sending RPC", arguments);
		this.socket.emit("rpc", rpcId, null, args);
	}

	rpc(rpcId : string, args : object) : Promise<void> {
		console.log("Sending RPC", arguments);
		let requestId = Math.floor(Math.random() * 1e8);

		let promise = new Promise<void>((resolve, reject) => {
			this.rpcCallbacks[requestId] = [resolve, reject];
			this.socket.emit("rpc", rpcId, requestId, args);
		});
		
		return promise;
	}
}

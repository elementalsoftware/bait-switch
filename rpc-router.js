"use strict";

const requireDir = require("require-dir");
let rpcHandlerImpls = requireDir("rpcHandlers");
let dataModel = require("./data-model");
let _ = require("lodash");

let rpcHandlers = new Map();
Object.entries(rpcHandlerImpls).forEach(function([id, impl]){
    rpcHandlers.set(id, new impl(dataModel));
});

module.exports = {
    hasRpc(rpcId){
        return rpcHandlers.has(rpcId);
    },
    execRpc(rpcId, rpcArgs, callback){
        rpcHandlers.get(rpcId).exec(rpcArgs, callback);
    },
    getDescriptor(){
        return _.fromPairs(Array.from(rpcHandlers.entries()).map(pair => {
            pair[1] = pair[1].getValidationSchema();
            return pair;
        }));
    }
};


"use strict";

const OPCODE_INPUT_SOURCE = 0;
const OPCODE_OUTPUT_RESOLUTION = 25;
const OPCODE_FREEZE = 89;
const OPCODE_BLANK = 90;

let formatGet = function(opCode){
    return "Y 1 " + opCode + "\r";
};

let formatSet = function(opCode, value){
    return "Y 0 " + opCode + " " + value + "\r";
};

class Vp728Driver {
    constructor(commDriver, dataModel, deviceId){



        let deviceConfig = dataModel.model.deviceConfig[deviceId];
        let deviceState = dataModel.model.deviceState[deviceId];

        deviceConfig.inputPortTypes = {
            0: "Input 1",
            1: "Input 2",
            2: "Input 3",
            3: "Input 4",
            4: "VGA 1",
            5: "VGA 2",
            6: "HDMI 1",
            7: "HDMI 2",
            8: "USB"
        };

        deviceConfig.resolutions = {
            0 : "Native HDMI",
            1 : "640x480@60Hz",
            2 : "640x480@75Hz",
            3 : "800x600@50Hz",
            4 : "800x600@60Hz",
            5 : "800x600@75Hz",
            6 : "1024x768@50Hz",
            7 : "1024x768@60Hz",
            8 : "1024x768@75Hz",
            9 : "1280x768@50Hz",
            10: "1280x768@60Hz",
            11: "1280x720@60Hz",
            12: "1280x800@60Hz",
            13: "1280x1024@50Hz",
            14: "1280x1024@60Hz",
            15: "1280x1024@75Hz",
            16: "1366x768@50Hz",
            17: "1366x768@60Hz",
            18: "1400x1050@50Hz",
            19: "1400x1050@60Hz",
            20: "1600x1200@50Hz",
            21: "1600x1200@60Hz",
            22: "1680x1050@60Hz",
            23: "1920x1080@60Hz",
            24: "1920x1200@60Hz",
            25: "480p@60Hz",
            26: "576p@60Hz",
            27: "720p@50Hz",
            28: "720p@60Hz",
            29: "1080i@50Hz",
            30: "1080i@60Hz",
            31: "1080p@50Hz",
            32: "1080p@60Hz",
            33: "480P@59.94Hz",
            34: "720P@59.94Hz",
            35: "1080i@59.94Hz",
            36: "1080P@23.98Hz",
            37: "1080P@29.97Hz",
            38: "1080P@59.94Hz"
        };

        deviceState.blank = false;
        deviceState.freeze = false;
        deviceState.resolution = 0;
        deviceState.outputPortMapping = [0];

        if(!deviceConfig.inputLabels){
            let inputLabels = new Array(Object.keys(deviceConfig.inputPortTypes).length);
            inputLabels.fill("");
            deviceConfig.inputLabels = inputLabels;
        }



        commDriver.on("connected", () => {
            //TODO: scan for current state before doing this
            deviceState.connected = true;
        });

        commDriver.on("disconnected", () => {
            deviceState.connected = false;
        });

        let messageHandlers = {
            [OPCODE_BLANK]: (blankState) => {
                deviceState.blank = blankState === 1;
            }
        };

        let receiveBuffer = "";
		let commandRegex = /Z [01] ([0-9]+) ([0-9]+)\r/;

        let parseCommand = function(){
            let match = receiveBuffer.match(commandRegex);
			if(match != null){
				let result = [parseInt(match[1]), parseInt(match[2])];
				receiveBuffer = receiveBuffer.substr(match.index + match[0].length);
				return result;
			}
            return null;
        };

        commDriver.on("dataReceived", function(data){
            receiveBuffer += data.toString();
            let command;
            while((command = parseCommand()) !== null){
                let [opCode, arg] = command;
                if(messageHandlers.hasOwnProperty(opCode)){
                    messageHandlers[opCode](arg);
                }
            }
        });

        dataModel.on("deviceState[" + deviceId + "].blank", function(events){
            let [path, oldVal, newVal] = events[events.length - 1];
            if(!deviceState.connected){
                return;
            }
            commDriver.sendRequest(formatSet(OPCODE_BLANK, newVal ? 1 : 0));
        });

        commDriver.connect();
    }

}

module.exports = Vp728Driver;

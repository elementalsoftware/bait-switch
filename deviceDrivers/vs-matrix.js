"use strict";
const _ = require("lodash");

class VsMatrixDriver {
    constructor(commDriver, dataModel, deviceId){
        let deviceConfig = dataModel.model.deviceConfig[deviceId];
        let deviceState = dataModel.model.deviceState[deviceId];

        if(!deviceConfig.inputLabels){
            let inputLabels = new Array(deviceConfig.inputs);
            inputLabels.fill("");
            deviceConfig.inputLabels = inputLabels;
        }

        let outputMapping = new Array(deviceConfig.outputs);
        outputMapping.fill(1);
        deviceState.outputPortMapping = outputMapping;

    }

}

module.exports = VsMatrixDriver;

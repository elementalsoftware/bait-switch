const proxyTree = require("@strangelytyped/proxy-tree-observable");

let baseModel = new proxyTree.DataModel({
    deviceState: [

    ],
    deviceConfig: [

    ]
});


module.exports = new proxyTree.PathSubscriber(new proxyTree.EventBuffer(baseModel));
Object.defineProperty(module.exports, "model", {
    get: () => baseModel.model
});
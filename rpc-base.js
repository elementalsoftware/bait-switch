"use strict";

const jsonSchemaValidate = require("jsonschema").validate;
const _ = require("lodash");

class RpcBase {
    constructor(dataModel){
        this._dataModel = dataModel;
    }

    error(message, callback){
        process.nextTick(callback, message);
    }

    success(callback){
        process.nextTick(callback, null);
    }

    getConfig(deviceId){
        if(deviceId < this._dataModel.model.deviceConfig.length){
            return this._dataModel.model.deviceConfig[deviceId];
        }
        return null;
    }

    getState(deviceId){
        if(deviceId < this._dataModel.model.deviceState.length){
            return this._dataModel.model.deviceState[deviceId];
        }
        return null;
    }

    getValidDeviceTypes(){
        return null;
    }

    getValidationSchema(){
        return null;
    }

    validate(execArgs, callback){
        //Validate against schema, if the operation provided one
        let validationSchema = this.getValidationSchema();
        if(validationSchema){
            if(!jsonSchemaValidate(execArgs, validationSchema)){
                this.error("Arguments to not match schema for this operation");
                return false;
            }
        }

        //Checks below here are application-specific (relating to device management)

        //Check the target device exists if the operation requires one
        //(schema should specify it required to ensure this check is invoked)
        let device, state;
        if(execArgs.hasOwnProperty("deviceId")){
            device = this.getConfig(execArgs.deviceId);
            if(!device){
                this.error("Could not find device " + execArgs.deviceId, callback);
                return false;
            }
            state = this.getState(execArgs.deviceId);
            if(!state){
                this.error("Could not find device state " + execArgs.deviceId, callback);
                return false;
            }
            //Disabled for testing
            //TODO: make toggle for headless testing
            /*if(!state.connected){
                this.error("Device " + execArgs.deviceId + " cannot service operations at this time", callback);
                return false;
            }*/
        }

        //validate the device type if the operation specifies what is valid
        let deviceValidation = this.getValidDeviceTypes();
        if(deviceValidation){
            let errMsg = "Device " + execArgs.deviceId + " is not a valid target for this operation";
            if(_.isArray(deviceValidation)){
                if(deviceValidation.indexOf(device.type) === -1){
                    this.error(errMsg, callback);
                    return false;
                }
            }else if(_.isString(deviceValidation)){
                if(device.type !== deviceValidation){
                    this.error(errMsg, callback);
                    return false;
                }
            }else if(_.isFunction(deviceValidation)){
                if(!deviceValidation(device.type)){
                    this.error(errMsg, callback);
                    return false;
                }
            }
        }

        return true;
    }
}


module.exports = RpcBase;